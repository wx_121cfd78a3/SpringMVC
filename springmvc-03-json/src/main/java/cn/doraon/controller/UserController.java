package cn.doraon.controller;

import cn.doraon.pojo.User;
import cn.doraon.utils.JsonUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController //类下面所有的方法都不会走视图解析
@Controller
public class UserController {
    @RequestMapping(value = "/j1")
//    @ResponseBody // 不走视图解析器
    public String json1() throws JsonProcessingException {
        User user = new User("小明", 24);
        return JsonUtils.getJson(user);
    }

    @RequestMapping(value = "/j2")
    public String json2() throws JsonProcessingException {
        List<User> list = new ArrayList<>();
        User user1 = new User("小明", 24);
        User user2 = new User("小红", 23);
        list.add(user1);
        list.add(user2);

        return JsonUtils.getJson(list);
    }

    @RequestMapping(value = "/j3")
    public String json3() throws JsonProcessingException {
        String sdf = "yyyy-MM-dd HH:mm:ss";

        return JsonUtils.getJson(new Date(), sdf);
    }
}
