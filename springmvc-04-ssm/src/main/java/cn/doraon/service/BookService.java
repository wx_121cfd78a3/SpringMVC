package cn.doraon.service;

import cn.doraon.pojo.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookService {
    int addBook(Book book);
    int deleteBook( int id);
    int updateBook(Book book);
    Book queryBookByID(int id);
    List<Book> queryAllBook();
    List<Book> queryBookByName(String bookName);

}
