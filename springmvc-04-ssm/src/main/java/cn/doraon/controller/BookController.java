package cn.doraon.controller;

import cn.doraon.pojo.Book;
import cn.doraon.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {
    @Resource(name = "bookServiceImpl")
    private BookService bookService;
    // 查询全部的书籍，返回到一个书籍展示页面
    @RequestMapping("/allBook")
    public String list(Model model) {
        List<Book> books = bookService.queryAllBook();
        model.addAttribute("list", books);
        return "allBook";
    }

    // 跳转到增加书籍页面
    @RequestMapping("/toAddBook")
    public String toAddBook() {
        return "addBook";
    }

    // 添加书籍请求
    @RequestMapping("/addBook")
    public String addBook(Book book) {
        System.out.println("addBook==>" + book);
        bookService.addBook(book);
        return "redirect:/book/allBook";
    }
    // 跳转到修改书籍页面
    @RequestMapping("/toUpdateBook")
    public String toUpdateBook(int id, Model model) {
        Book book = bookService.queryBookByID(id);
        model.addAttribute("queryBook", book);
        return "updateBook";
    }

    // 修改书籍
    @RequestMapping("/updateBook")
    public String updateBook(Book book) {
        System.out.println("updateBook==>" + book);
        bookService.updateBook(book);
        return "redirect:/book/allBook";
    }


    // 查询书籍
    @RequestMapping("/queryBookByName")
    public String queryBook(String bookName, Model model) {
        List<Book> books = bookService.queryBookByName(bookName);
        model.addAttribute("list", books);
        return "allBook";
    }
}
