package cn.doraon.dao;

import cn.doraon.pojo.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    int addBook(Book book);
    int deleteBook(@Param("bookID") int id);
    int updateBook(Book book);
    Book queryBookByID(@Param("bookID") int id);
    List<Book> queryAllBook();
    List<Book> queryBookByName(@Param("bookName")String bookName);
}
