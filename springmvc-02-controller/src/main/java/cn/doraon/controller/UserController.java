package cn.doraon.controller;

import cn.doraon.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
public class UserController {
    @GetMapping("/t1")
    public String test1(@RequestParam("username") String name, Model model) {

        model.addAttribute("msg", "接收的前端参数为：" + name);
        return "test";
    }
    // 前端接收的是一个对象
    @GetMapping("/t2")
    public String test2(User user) {
        System.out.println(user);
        return "test";

    }
}
